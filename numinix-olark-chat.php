<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           Plugin_Name
 *
 * @wordpress-plugin
 * Plugin Name:       Numinix Olark Chat
 * Plugin URI:        https://www.numinix.com/plugins/woocommerce-plugins
 * Description:       Increase your conversion rates by keeping in contact with your customers with this lightweight, feature-rich live chat widget by Olark.
 * Version:           1.0.0
 * Author:            Numinix
 * Author URI:        https://bitbucket.org/numinix/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       numinix-olark-chat
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-plugin-name-activator.php
 */
function activate_numinix_olark_chat() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-numinix-olark-chat-activator.php';
	Numinix_Olark_Chat_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-plugin-name-deactivator.php
 */
function deactivate_numinix_olark_chat() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-numinix-olark-chat-deactivator.php';
	Numinix_Olark_Chat_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_numinix_olark_chat' );
register_deactivation_hook( __FILE__, 'deactivate_numinix_olark_chat' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-numinix-olark-chat.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_numinix_olark_chat() {

	$plugin = new Numinix_Olark_Chat();
	$plugin->run();

}

run_numinix_olark_chat();