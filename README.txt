=== Olark for WP ===
Contributors: chien chou
Donate link:
Tags: olark, live chat, numinix, olark live chat
Requires at least: 3.1
Tested up to: 4.8.3
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Add olark live chat to your site!!

== Description ==

Increase your conversion rates by keeping in contact with your customers with this lightweight, feature-rich live chat widget by Olark. 

The Olark Chat Widget works on all pages (SSL and non-SSL) and connects to most major desktop chat messenger programs, including Google Talk. The single piece of JavaScript added to your website will allow you to see what customers are doing on your website, and give you the ability to message them at any time. You can even message the customer when you see them having problems completing a purchase in your store. 

Olark integrates with many CRM platforms and Help Desk software. With its reporting functionality, you can tell who your customers are, the most active times of day on your website and track how well your operators are performing. Further, customizing Olark is simple. There are multiple themes and options to choose from to get the widget looking just right.

With all of these features and a low monthly price, Olark is our number one pick when it comes to adding live chat to e-commerce websites. In fact, we're using it on Numinix.com right now. Send us a message if you have questions or just want to say hi!

== Installation ==

1. Extract all files from the ZIP archive, making sure to keep the file structure intact.
2. Upload the `olark-for-wp` folder to the `/wp-content/plugins/` directory.
3. Activate the plugin through the `Plugins` menu in WordPress.
4. Go to the `Olark for WP` menu which is located under the `Settings` menu in the admin interface.
5. Enter your Olark account ID and then go to the frontend of your website to see your new chat widget in action!

**See Also:** ["Installing Plugins" article on the WP Codex](http://codex.wordpress.org/Managing_Plugins#Installing_Plugins)