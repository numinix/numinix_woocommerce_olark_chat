<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Numinix_Olark_Chat
 * @subpackage Numinix_Olark_Chat/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Numinix_Olark_Chat
 * @subpackage Numinix_Olark_Chat/includes
 * @author     Your Name <email@example.com>
 */
class Numinix_Olark_Chat_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
