<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Numinix_Olark_Chat
 * @subpackage Numinix_Olark_Chat/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Numinix_Olark_Chat
 * @subpackage Numinix_Olark_Chat/includes
 * @author     Your Name <email@example.com>
 */
class Numinix_Olark_Chat_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
