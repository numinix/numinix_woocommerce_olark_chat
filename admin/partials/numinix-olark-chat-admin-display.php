<?php
/**
*
* admin/partials/wp-cbf-admin-display.php - Don't add this comment
*
**/
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">

    <h2><?php echo esc_html(get_admin_page_title()); ?></h2>

    <h2 class="nav-tab-wrapper">Olark Live Chat Configuration</h2>

    <form method="post" name="cleanup_options" action="options.php">

    <?php
        //Grab all options
        $options = get_option($this->plugin_name);

        $olark_status = $options['olark_status'];
        $olark_site_id = $options['olark_site_id'];
        
        $olark_product_category = $options['olark_product_category'];
        $olark_product = $options['olark_product'];
        $olark_shopping_cart = $options['olark_shopping_cart'];
        $olark_checkout_page = $options['olark_checkout_page'];
        $olark_home_page = $options['olark_home_page'];
        if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ){
            die('<b>PLease install woocommerce first before installing this plugin</b>');
        }
    ?>

    <?php
        settings_fields($this->plugin_name);
        do_settings_sections($this->plugin_name);
    ?>
    
    <fieldset>
        <legend class="screen-reader-text"><span>Enable Olark Live Chat</span></legend>
        <label for="<?php echo $this->plugin_name; ?>-olark_status">
            <span><?php esc_attr_e('Enable Olark Live Chat?', $this->plugin_name); ?></span><br>
            <input type="checkbox" id="<?php echo $this->plugin_name; ?>-olark_status" name="<?php echo $this->plugin_name; ?>[olark_status]" value="1" <?php checked($olark_status, 1); ?> />
        </label>
    </fieldset>

    <fieldset>
        <legend class="screen-reader-text">
            <span>Site ID</span>
        </legend>
        <label for="<?php echo $this->plugin_name; ?>-olark_site_id">
            <span><?php esc_attr_e('Add your Olark Site ID:', $this->plugin_name); ?></span><br>
            <input type="text" id="<?php echo $this->plugin_name; ?>-olark_site_id" name="<?php echo $this->plugin_name; ?>[olark_site_id]" value="<?php echo $olark_site_id; ?>"/>
        </label>
    </fieldset>
    
    <br>
    
    <h2 class="nav-tab-wrapper">Olark Register & Chat Links</h2>
    
    <p>Register an account <a href='https://www.olark.com/pricing?ref=jefflew?'>here</a> if you don't have an account</p>
    <p>Click <a href='https://www.olark.com/pricing?ref=jefflew?'>here</a> to access the olark dashboard</p>
    
    <br>
    
    <h2 class="nav-tab-wrapper">Olark Live Chat Pages (Leave all unchecked to include all pages)</h2>
    
    <fieldset>
        <legend class="screen-reader-text">
            <span>Product Category</span>
        </legend>
        <label for="<?php echo $this->plugin_name; ?>-olark_product_category">
            <span><?php esc_attr_e('Product Category:', $this->plugin_name); ?></span>
            <input type="checkbox" id="<?php echo $this->plugin_name; ?>-olark_product_category" name="<?php echo $this->plugin_name; ?>[olark_product_category]" value="1" <?php checked($olark_product_category, 1); ?> />
        </label>
    </fieldset>
    <fieldset>
        <legend class="screen-reader-text">
            <span>Product</span>
        </legend>
        <label for="<?php echo $this->plugin_name; ?>-olark_product">
            <span><?php esc_attr_e('Product:', $this->plugin_name); ?></span>
             <input type="checkbox" id="<?php echo $this->plugin_name; ?>-olark_product" name="<?php echo $this->plugin_name; ?>[olark_product]" value="1" <?php checked($olark_product, 1); ?> />
        </label>
    </fieldset>
    <fieldset>
        <legend class="screen-reader-text">
            <span>Shopping Cart</span>
        </legend>
        <label for="<?php echo $this->plugin_name; ?>-olark_shopping_cart">
            <span><?php esc_attr_e('Shopping Cart:', $this->plugin_name); ?></span>
            <input type="checkbox" id="<?php echo $this->plugin_name; ?>-olark_shopping_cart" name="<?php echo $this->plugin_name; ?>[olark_shopping_cart]" value="1" <?php checked($olark_shopping_cart, 1); ?> />
        </label>
    </fieldset>
    <fieldset>
        <legend class="screen-reader-text">
            <span>Checkout Page</span>
        </legend>
        <label for="<?php echo $this->plugin_name; ?>-olark_checkout_page">
            <span><?php esc_attr_e('Checkout Page:', $this->plugin_name); ?></span>
            <input type="checkbox" id="<?php echo $this->plugin_name; ?>-olark_checkout_page" name="<?php echo $this->plugin_name; ?>[olark_checkout_page]" value="1" <?php checked($olark_checkout_page, 1); ?> />
        </label>
    </fieldset>
    <fieldset>
        <legend class="screen-reader-text">
            <span>Home Page</span>
        </legend>
        <label for="<?php echo $this->plugin_name; ?>-olark_home_page">
            <span><?php esc_attr_e('Home Page:', $this->plugin_name); ?></span>
            <input type="checkbox" id="<?php echo $this->plugin_name; ?>-olark_home_page" name="<?php echo $this->plugin_name; ?>[olark_home_page]" value="1" <?php checked($olark_home_page, 1); ?> />
        </label>
    </fieldset>
    
    

    <?php submit_button('Save all changes', 'primary','submit', TRUE); ?>


</div>