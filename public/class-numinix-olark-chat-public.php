<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Numinix_Olark_Chat
 * @subpackage Numinix_Olark_Chat/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Numinix_Olark_Chat
 * @subpackage Numinix_Olark_Chat/public
 * @author     Your Name <email@example.com>
 */
class Numinix_Olark_Chat_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->olark_options = get_option($this->plugin_name);

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Numinix_Olark_Chat_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Numinix_Olark_Chat_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/numinix-olark-chat-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
        global $current_user;
		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Numinix_Olark_Chat_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Numinix_Olark_Chat_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		 
        if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ){
            if($this->olark_options['olark_status']){
                if($this->olark_options['olark_product_category'] || $this->olark_options['olark_product'] || $this->olark_options['olark_shopping_cart'] || $this->olark_options['olark_checkout_page'] || $this->olark_options['olark_home_page']){
                    $return = true;
                    
            		if($this->olark_options['olark_product_category'] ){
            		    if(is_product_category()) $return = false;
            		}
            		
            		if($this->olark_options['olark_product']){
            		    if(is_product()) $return = false;
            		}
            		
            		if($this->olark_options['olark_shopping_cart']){
            		    if(is_cart()) $return = false;
            		}
            		
            		if($this->olark_options['olark_checkout_page']){
            		    if(is_checkout()) $return = false;
            		}
            		
            		if($this->olark_options['olark_home_page']){
            		    if(is_front_page()) $return = false;
            		}
            		
            		if($return){
            		    return;
            		}
                }
        		
        		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/numinix-olark-chat-public.js', array( 'jquery' ), $this->version, false );
        		
        		$configuration = array(
        		  'site_ID' => $this->olark_options['olark_site_id']
        		);
        		
        		if(0 != $current_user->ID){
        		    if($current_user->user_firstname != 0){
        		        $configuration['customer_name'] = $current_user->display_name; 
        		    } else {
        		        $configuration['customer_name'] = $current_user->user_firstname . ' ' . $current_user->user_lastname;
        		    }
        		    
        		    $configuration['customer_email'] = $current_user->user_email;
        		    
        		    $customer_orders = get_posts( array(
                        'numberposts' => -1,
                        'meta_key'    => '_customer_user',
                        'meta_value'  => get_current_user_id(),
                        'post_type'   => wc_get_order_types(),
                        'post_status' => array_keys( wc_get_order_statuses() ),
                        'order', 'DESC'
                    ) );
                
                    $last_order_id = $customer_orders[0]->ID;
                    $order = wc_get_order( $last_order_id );
        		    
        		    $configuration['orders_total'] = count( $customer_orders );
        		    $configuration['last_order_date'] = $order->get_date_created();
        		    $configuration['last_order_total'] = $order->get_total();
        		}
        		
        		wp_localize_script( $this->plugin_name, 'olark_vars', $configuration ); 
            }
    	}
	}

}
