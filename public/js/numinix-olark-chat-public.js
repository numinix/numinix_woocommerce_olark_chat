;(function(o,l,a,r,k,y){if(o.olark)return; r="script";y=l.createElement(r);r=l.getElementsByTagName(r)[0]; y.async=1;y.src="//"+a;r.parentNode.insertBefore(y,r); y=o.olark=function(){k.s.push(arguments);k.t.push(+new Date)}; y.extend=function(i,j){y("extend",i,j)}; y.identify=function(i){y("identify",k.i=i)}; y.configure=function(i,j){y("configure",i,j);k.c[i]=j}; k=y._={s:[],t:[+new Date],c:{},l:a}; })(window,document,"static.olark.com/jsclient/loader.js");
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify(olark_vars.site_ID);

console.log('[Olark] site ID: ' + olark_vars.site_ID);
console.log('[Olark] order total: ' + olark_vars.orders_total);

if(olark_vars.customer_name != 0){
    console.log('[Olark] customer name: ' + olark_vars.customer_name);
    olark('api.chat.updateVisitorNickname', {
        snippet: olark_vars.customer_name
    });
    olark('api.visitor.updateFullName', {
        fullName: olark_vars.customer_name
    });
    olark('api.chat.updateVisitorStatus', {
        snippet: ['Last order date : ' + olark_vars.last_order_date.date,
                  'Last order amount : ' + olark_vars.last_order_total,
                  'Total number of orders : ' + olark_vars.orders_total,
                  'Email Address : ' + olark_vars.customer_email,
                 ]
    });
}